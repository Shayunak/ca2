
module activationFunction #(parameter m = 8)(ready, inp, out);
  input ready;
  input [m:0]inp; 
  output [m:0]out;
  
  assign out = (ready) ? ((inp[m] == 0) ? inp : 0) : {(m + 1){1'bZ}};
  
endmodule