module adder#(parameter len = 7)(a, b, s);
    input [len:0]a;
    input [len:0]b;
    output [len:0]s;
    
    assign s = a + b;
endmodule