module controller #(parameter dataSize = 1, parameter regSize = 1)(start, clk, rst, ready, select, clr);
  input start, clk, rst;
  output reg ready, clr;
  output reg [regSize:0]select;
  
  reg [regSize:0]ps;
  reg [regSize:0]ns;
  
  always@(ps, start)begin
    select <= 0;
    ns <= 0;
    ready <= 0;
    clr <= 0;
    
    if (ps == 0) begin ns <= start ? 1 : 0; ready <= 1'b1; clr <= start ? 1 : 0; end
    else if(ps == dataSize) begin ns <= 0; select <= ps - 1; end
   else if(ps > 0)begin ns <= (ps + 1); select <= ps - 1; end
    else ps <= 0;
      
  end
  
  always@(clk, rst)begin
    if (rst) ps <= 0;
    else ps <= ns;
  end
    
endmodule
