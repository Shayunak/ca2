module vector #(parameter dataSize = 3)(select, out);
  	input [dataSize:0]select;
 	output [7:0]out;
  
  	reg [7:0]mem[0:dataSize];
 	initial begin
	#100
    	$readmemh("./data.hex", mem);
	end
	
	assign out = mem[select];
endmodule
