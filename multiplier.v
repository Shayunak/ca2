module multiplier #(parameter accuSize = 7) (A , B , out);
	input[7:0] A,B;
	wire[15:0] C;
	output[accuSize:0] out;
	wire sign;
	wire[accuSize:0] assignOut;
	wire[1:0] A3,B3;
	wire[3:0] A0B0,A0B1,A1B0,A1B1,A2B0,A0B2,A2B1,A1B2,A3B0,A0B3,A2B2,A3B1,A1B3,A3B2,A2B3,A3B3;
	//
	assign A3 = {1'b0 , A[6]};
	assign B3 = {1'b0 , B[6]};
	assign sign = A[7] ^ B[7]; 
	//
	MultiplierTwoInTwo mult1 (.A(A[1:0]) , .B(B[1:0]) , .C(A0B0) );
	MultiplierTwoInTwo mult2 (.A(A[1:0]) , .B(B[3:2]) , .C(A0B1) );
	MultiplierTwoInTwo mult3 (.A(A[3:2]) , .B(B[1:0]) , .C(A1B0) );
	MultiplierTwoInTwo mult4 (.A(A[3:2]) , .B(B[3:2]) , .C(A1B1) );
	MultiplierTwoInTwo mult5 (.A(A[5:4]) , .B(B[1:0]) , .C(A2B0) );
	MultiplierTwoInTwo mult6 (.A(A[1:0]) , .B(B[5:4]) , .C(A0B2) );
	MultiplierTwoInTwo mult7 (.A(A[5:4]) , .B(B[3:2]) , .C(A2B1) );
	MultiplierTwoInTwo mult8 (.A(A[3:2]) , .B(B[5:4]) , .C(A1B2) );
	MultiplierTwoInTwo mult9 (.A(A3) , .B(B[1:0]) , .C(A3B0) );
	MultiplierTwoInTwo mult10 (.A(A[1:0]) , .B(B3) , .C(A0B3) );
	MultiplierTwoInTwo mult11 (.A(A[5:4]) , .B(B[5:4]) , .C(A2B2) );
	MultiplierTwoInTwo mult12 (.A(A3) , .B(B[3:2]) , .C(A3B1) );
	MultiplierTwoInTwo mult13 (.A(A[3:2]) , .B(B3) , .C(A1B3) );
	MultiplierTwoInTwo mult14 (.A(A3) , .B(B[5:4]) , .C(A3B2) );
	MultiplierTwoInTwo mult15 (.A(A[5:4]) , .B(B3) , .C(A2B3) );
	MultiplierTwoInTwo mult16 (.A(A3) , .B(B3) , .C(A3B3) );
	//
	assign C = {12'b0,A0B0} + {10'b0,{A1B0,2'b0}} + {10'b0,{A0B1,2'b0}}
		+ {8'b0,{A1B1,4'b0}} + {8'b0,{A2B0,4'b0}} + {8'b0,{A0B2,4'b0}}
		+ {6'b0,{A2B1,6'b0}} + {6'b0,{A1B2,6'b0}} + {6'b0,{A3B0,6'b0}}
		+ {6'b0,{A0B3,6'b0}} + {4'b0,{A2B2,8'b0}} + {4'b0,{A3B1,8'b0}}
		+ {4'b0,{A1B3,8'b0}} + {2'b0,{A3B2,10'b0}} + {2'b0,{A2B3,10'b0}}
		+ {A3B3,12'b0};
	//
	assign assignOut = C;
	assign out = sign ? -assignOut : assignOut;
endmodule
