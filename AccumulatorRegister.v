module accRegister#(parameter size = 18)(inp, clk, clr, out);
  input [size:0]inp;
  input clk, clr;
  output reg [size:0] out;
  
  always@(posedge clk, clr) begin
    if (clr) out <= {(size){1'b0}};
    else  out <= inp;
  end
  
endmodule
