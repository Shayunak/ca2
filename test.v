module activationFunctionTB();
  reg ready;
  reg [10:0]inp;
  wire [10:0]out;
  
  activationFunction acf(.ready(ready), .inp(inp), .out(out));
  defparam acf.m = 10;
  
  initial begin
    ready = 1'b0;
    inp = 11'b01111111011;
    #100
    ready = 1'b1;
    #100
    ready = 1'b0;
    inp = 11'b10000011001;
    #100
    ready = 1'b1;
    #100
    $stop;
  end
endmodule

module ControllerTB();
  reg start, clk, rst;
  wire ready, clr;
  wire [1:0] select;
  
  controller cu(.start(start), .clk(clk), .rst(rst), .ready(ready), .select(select), .clr(clr));
  defparam cu.dataSize = 3;
  defparam cu.regSize = 1;
  
  initial begin
    clk = 1'b0;
    repeat(30) #100 clk = ~clk;    
  end
  
  initial begin
    rst = 1'b1;
    #100
    rst = 1'b0;
    #100
    start = 1'b1;
    #150
    start = 1'b0;
  end  
endmodule

module vectorTB();
	reg[7:0]sel;
	wire[7:0] out;
	vector vec (.select(sel) , .out(out));
	defparam vec.dataSize = 7;
	initial begin
		#300
		sel = 3;
		#100
		sel = 5;
		#100
		$stop;
	end
endmodule

module adderTB();
	reg[19:0] a,b;
	wire[19:0] s;
	adder adr (.a(a) , .b(b) , .s(s));
	defparam adr.len = 19;
	initial begin
	a = 1;
	b = 2;
	#100
	a = -a;
	b = b;
	#100
	$stop;
	end
endmodule

module MultTB();
	reg[7:0] A,B;
	wire[19:0] C;
	multiplier mult (.A(A) , .B(B) , .out(C) );
	defparam mult.accuSize = 19;
	initial begin
	#50
	A = 8'b10001110;
	B = 8'b11110000;
	#50
	$stop;
	end
endmodule
